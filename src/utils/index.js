export const ArrayState = {
    update: (arrayState, index, data) => [
        ...arrayState.slice(0, index),
        data,
        ...arrayState.slice(index + 1),
    ],
    remove:  (arrayState, index) => [
        ...arrayState.slice(0, index),            
        ...arrayState.slice(index + 1),
    ],
    push: (arrayState, data) => [
        ...arrayState,
        data
    ],
};