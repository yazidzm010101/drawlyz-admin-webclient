// mediocre
import React, { useContext, useState } from "react";
import axios from "axios";
import { AppContext } from "../App";


// components
import AppLogo from "../components/AppLogo";
import Toast from "../components/Toast";
import { MDBInput, MDBBtn } from "mdb-react-ui-kit";

// stylesheets
import "../stylesheets/signin.css";

export default function SigninPage() 
{
    const { baseUrl, setUser, setToast } = useContext(AppContext);
    const [form, setForm] = useState({ username: "", password: "" });

    function updateForm(e) {
        setForm({ ...form, [e.target.name]: e.target.value });
    }

    function submitForm(e) {
        e.preventDefault();        
        axios.post(baseUrl + '/signIn', form, { withCredentials: true })
            .then(response => {
                if (response.status === 200) {
                    setUser(response.data.user);
                    setToast(response.data.message);
                    return;
                }
                setToast('An error happened when trying to sign in');
            })
            .catch(err => {
                if (err.response) {
                    if(err.response.data.message) {
                        setToast(err.response.data.message);
                        return;
                    }
                }
                setToast('An error happened when trying to sign in');
            });
    }

    return (
        <div className="bg-dark" id="background">
            <form
                id="sign-in"
                className="mx-auto d-flex flex-column justify-content-center w-100 p-4 p-lg-5 text-light"
                onSubmit={submitForm}
            >
                <Toast context={AppContext} position="absolute" />
                <div className="py-3 text-center">
                    <h5 className="my-1">Masuk Administrator</h5>
                    <AppLogo />
                </div>
                <div className="py-3">
                    <MDBInput
                        className="my-3"
                        label="Username"
                        id="username"
                        name="username"
                        type="text"
                        onChange={updateForm}
                    />
                    <MDBInput
                        className="my-3"
                        label="Password"
                        id="password"
                        name="password"
                        type="password"
                        onChange={updateForm}
                    />
                    <MDBBtn className="my-3 w-100" type="submit">
                        Sign in
                    </MDBBtn>
                </div>
            </form>
        </div>
    );
}
