import AppLogo from "../../../components/AppLogo";
import { MDBContainer } from "mdb-react-ui-kit";

export default function AboutPage() {
    return (
        <MDBContainer
            fluid
            className="text-center h-100"
            style={{ maxWidth: "576px" }}
        >
            <div className="py-5">
                <AppLogo />
            </div>
            <p className="py-3">
                <strong>
                    DRAWLYZ
                    <sup className="ms-1 text-primary-variant">admin</sup>
                </strong>
                is a web-based application for managing data accessed by{" "}
                <strong>DRAWLYZ</strong> on Android to make it easier for people
                to choose a graphic tablet based on the desired criteria. Four
                maximum criteria (size, working area, pressure levels, and
                prices) can be used with each degree of membership using Fuzzy
                Logic calculations.
            </p>
            <p className="my-0">
                <img
                    className="img-fluid p-1"
                    src="https://upload.wikimedia.org/wikipedia/commons/4/4e/Gmail_Icon.png"
                    alt="gmail"
                    style={{ height: "24px" }}
                ></img>
                yazidzm.developer@gmail.com
            </p>
            <p className="my-0">
                <img
                    className="img-fluid p-1"
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/210px-GitLab_Logo.svg.png"
                    alt="gmail"
                    style={{ height: "24px" }}
                ></img>
                @yazidzm010101
            </p>
        </MDBContainer>
    );
}
