// main
import React from "react";

// components
import CriteriaChart from "./CriteriaChart";
import {
    MDBCard,
    MDBCardTitle,    
    MDBCardBody,
    MDBBtn,
} from "mdb-react-ui-kit";

export default function CriteriaCard({ data, showForm }) {
    const title = (
        <MDBCardTitle className="fw-bold">
            {data.name}
        </MDBCardTitle>
    );        

    return (
        <MDBCard className="overflow-hidden my-2 condensed position-relative">
            <MDBBtn
                className="text-light fw-bold p-3 m-0 w-100 d-flex justify-content-end d-inline-block shadow-0 position-absolute top-0 end-0 bottom-0 start-0"
                color="transparent"
                onClick={showForm}
                style={{ zIndex: "3" }}
            ></MDBBtn>
            <MDBCardBody>
                {title}
                <CriteriaChart categories={data.categories} style={{ maxHeight: '300px' }}/>
            </MDBCardBody>
        </MDBCard>
    );
}
