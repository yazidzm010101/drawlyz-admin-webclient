// main
import React, { useState, useEffect, useContext } from "react";
import { AppContext } from "../../../App";
import { ArrayState } from "../../../utils";

// components
import CategoryForm from "./CategoryForm";
import CriteriaChart from "./CriteriaChart";
import {
    MDBBtn,
    MDBModal,
    MDBModalDialog,
    MDBModalContent,
    MDBModalHeader,
    MDBModalTitle,
    MDBModalBody,
    MDBModalFooter,
    MDBTabs,
    MDBTabsItem,
    MDBTabsLink,
    MDBTabsContent,
    MDBTabsPane,
} from "mdb-react-ui-kit";

export default function CriteriaForm({ data, reloadData, toggleShow }) {
    const { apiRequest, setToast } = useContext(AppContext);
    const [form, setForm] = useState(data);
    const [show, setShow] = useState(false);
    const [activeTab, setActiveTab] = useState(0);
    const title = data.name;

    function addCategory() {
        const categories = ArrayState.push(form.categories, {
            name: "New Category",
            sets: [{ x: "", y: "" }],
        });
        setForm({ ...form, categories });
        setActiveTab(form.categories.length);
    }

    function updateCategory(category, index) {
        const categories = ArrayState.update(form.categories, index, category);
        setForm({ ...form, categories });
    }

    function removeCategory(iCategory) {
        const categories = ArrayState.remove(form.categories, iCategory);
        setForm({ ...form, categories });
    }

    function submitForm(e) {
        e.preventDefault();
        apiRequest(
            { method: "PUT", endpoint: "/criterias", data: form },
            (err, response) => {
                if (err) {
                    !response && setToast("Failed to save changes");
                    return;
                }
                if (response.status === 200) {
                    setToast("Changes saved successfully");
                    hideForm();
                    setTimeout(() => {
                        reloadData();
                    }, 200);
                }
            }
        );
    }

    function hideForm() {
        setShow(false);
        setTimeout(() => {
            toggleShow();
        }, 200);
    }

    useEffect(() => {
        setTimeout(() => {
            setShow(true);
        }, 5);
    }, []);

    const CategoryTab = ({ name, index }) => (
        <MDBTabsItem>
            <MDBTabsLink
                active={index === activeTab}
                onClick={() => setActiveTab(index)}
            >
                {name}
                {index === activeTab ? (
                    <MDBBtn
                        color="transparent"
                        className="p-0 ms-1 align-baseline shadow-0 text-danger"
                        onClick={() => removeCategory(index)}
                        style={{
                            lineHeight: 0,
                            color: "#d32f2f",
                        }}
                    >
                        <i className="fas fa-times"></i>
                    </MDBBtn>
                ) : null}
            </MDBTabsLink>
        </MDBTabsItem>
    );

    return (
        <form onSubmit={submitForm}>
            <MDBModal show={show} onClick={hideForm}>
                <MDBModalDialog
                    className="modal-fullscreen-sm-down"
                    onClick={(e) => e.stopPropagation()}
                >
                    <MDBModalContent>
                        <MDBModalHeader className="flex-wrap pb-0">
                            <MDBModalTitle>{title}</MDBModalTitle>
                            <MDBBtn
                                color="none"
                                className="btn-close"
                                onClick={hideForm}
                            ></MDBBtn>
                            <CriteriaChart
                                categories={form.categories}
                                style={{ maxHeight: "480px" }}
                            />
                            <div className="tabbable w-100">
                                <MDBTabs className="d-flex">
                                    {form.categories.map((item, index) => (
                                        <CategoryTab
                                            key={"categories" + item._id}
                                            name={item.name}
                                            index={index}
                                        />
                                    ))}
                                    <MDBTabsItem>
                                        <MDBBtn
                                            color="transparent"
                                            rounded
                                            className="px-3 shadow-0"
                                            onClick={addCategory}
                                        >
                                            <i className="fa fa-plus"></i>
                                        </MDBBtn>
                                    </MDBTabsItem>
                                </MDBTabs>
                            </div>
                        </MDBModalHeader>
                        <MDBModalBody>
                            <MDBTabsContent>
                                {form.categories.map((item, index) => (
                                    <MDBTabsPane
                                        key={item._id}
                                        show={index === activeTab}
                                    >
                                        <CategoryForm
                                            category={item}
                                            updateCategory={(data) =>
                                                updateCategory(data, index)
                                            }
                                        />
                                    </MDBTabsPane>
                                ))}
                            </MDBTabsContent>
                        </MDBModalBody>
                        <MDBModalFooter>
                            <MDBBtn color="dark" onClick={hideForm}>
                                Close
                            </MDBBtn>
                            <MDBBtn type="submit">Save Changes</MDBBtn>
                        </MDBModalFooter>
                    </MDBModalContent>
                </MDBModalDialog>
            </MDBModal>
        </form>
    );
}
