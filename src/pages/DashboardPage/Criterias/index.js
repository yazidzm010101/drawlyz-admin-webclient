// main
import React, { useContext, useState, useEffect } from "react";
import { AppContext } from "../../../App";

// components
import Loading from "../../../components/Loading";
import CriteriaCard from "./CriteriaCard";
import CriteriaForm from "./CriteriaForm";
import { MDBContainer, MDBRow, MDBCol } from "mdb-react-ui-kit";

export default function CriteriaPage() {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState([]);
    const [form, setForm] = useState({ isEnabled: false, index: -1 });
    const { apiRequest } = useContext(AppContext);

    function loadData() {
        setLoading(true);
        setError(false);
        setData([]);
        apiRequest({
            method: "get",
            endpoint: "/criterias",
            setData,
            setError,
            setLoading,
        });
    }

    function showForm(index) {
        if (form.isEnabled) {
            setForm({ isEnabled: false, index });
            return;
        }
        setForm({ isEnabled: true, index });
    }

    useEffect(() => {
        loadData();
        return () => {
            setError(true);
        };
    }, []);

    return (
        <Loading full loading={loading} error={error} reloadData={loadData}>
            <MDBContainer fluid className="py-2 py-lg-4 px-3 px-lg-4">
                {form.isEnabled && (
                    <CriteriaForm
                        show={form.isEnabled}
                        data={data[form.index]}
                        reloadData={loadData}
                        toggleShow={() => showForm(form.index)}
                    />
                )}
                <MDBRow>
                    {data.map((item, index) => (
                        <MDBCol key={"criteriaCard" + item._id} lg="6">
                            <CriteriaCard
                                data={item}
                                showForm={() => showForm(index)}
                            />
                        </MDBCol>
                    ))}
                </MDBRow>
            </MDBContainer>
        </Loading>
    );
}
