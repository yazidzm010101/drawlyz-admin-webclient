// main
import { useState, useEffect } from "react";
import { ArrayState } from "../../../utils";

// components
import {
    MDBRow,
    MDBCol,
    MDBInput,
    MDBBtn,    
} from "mdb-react-ui-kit";

export default function CategoryForm({ category, updateCategory }) {
    
    const [form, setForm] = useState(category);    

    function handleField(e) {                
        setForm({
            ...form,  
            [e.target.name]: e.target.value
        });        
    }

    function addSets() {
        const sets = ArrayState.push(category.sets, {
            x: '',
            y: '',
        });        
        setForm({...form, sets });        
    }

    function updateSets(e, index) {
        const sets = ArrayState.update(category.sets, index, {
            ...category.sets[index],
            [e.target.name]: parseFloat(e.target.value),
        });
        setForm({...form, sets });        
    }

    function removeSets(index) {
        const sets = ArrayState.remove(category.sets, index);
        setForm({...form, sets });        
    }

    useEffect(() => {     
        updateCategory(form);
    }, [form]);
            
    return (
        <>
            <MDBInput
                required
                type="text"                
                className="active my-3"
                label="Category Name"
                name="name"
                value={form.name}
                onChange={handleField}
            />
            {form.sets.map((item, index) => (
                <MDBRow
                    key={"form-sets" + item._id}
                    className="g-3 mb-3"
                >
                    <MDBCol size="6">
                        <MDBInput
                            type="number"
                            className="active"
                            label="X"
                            name="x"
                            required
                            value={item.x}
                            onChange={(e) => updateSets(e,index)
                            }
                        />
                    </MDBCol>
                    <MDBCol size="5">
                        <MDBInput
                            type="number"
                            className="active"
                            label="Y"
                            name="y"
                            required
                            value={item.y}
                            onChange={(e) => updateSets(e,index)
                            }
                        />
                    </MDBCol>
                    <MDBCol size="1">
                        <MDBBtn
                            color="link"
                            className="text-danger px-1"
                            disabled={!form.sets.length > 0}
                            onClick={() =>
                                form.sets.length > 0 &&
                                removeSets(index)
                            }                            
                        >
                            <i className="fas fa-times"></i>
                        </MDBBtn>
                    </MDBCol>
                </MDBRow>
            ))}
            <div className="text-center">
                <MDBBtn
                    color="link"
                    className="text-dark"
                    onClick={addSets}
                >
                    <i className="fa fa-plus me-1"></i> Add
                    new set
                </MDBBtn>
            </div>
        </>
    );
}