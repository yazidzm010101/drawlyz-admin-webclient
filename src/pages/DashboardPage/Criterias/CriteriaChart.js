import { Scatter } from "react-chartjs-2";

export default function CriteriaChart({ categories, ...rest }) {
    const scatterData = {
        datasets: categories.map((item, index) => {
            const h = (120 / categories.length) * index;
            const color = `hsla(${h}, 70%, 90%, 0.95)`;
            const backgroundColor = `hsla(${h}, 90%, 70%, 0.35)`;
            return {
                label: item.name,
                data: item.sets,
                fill: true,
                showLine: true,
                backgroundColor,
                pointBorderColor: color,
                pointBackgroundColor: color,
            };
        }),
    };

    const scatterOptions = {
        animation: {
            duration: 0,
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    return (
        <Scatter
            type="area"
            data={scatterData}
            options={scatterOptions}
            {...rest}
        />
    );
}
