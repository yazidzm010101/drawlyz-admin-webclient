// main
import React, { useState, useContext, useEffect } from "react";
import { AppContext } from "../../../App";

// components
import Loading from "../../../components/Loading";
import {    
    MDBListGroup,
    MDBListGroupItem,
    MDBBtn,
    MDBRow,
    MDBCol,
} from "mdb-react-ui-kit";

export default function HistoryList() {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState([]);    
    const { apiRequest, setToast } = useContext(AppContext);

    function loadData() {
        setLoading(true);
        setError(false);
        setData([]);
        apiRequest({
            method: "get",
            endpoint: "/parsehubs/history",
            setData: (data) => {
                console.log(data);
                setData(data);
            },
            setError,
            setLoading,
        });
    }     
    
    function History({ item }) {
        var details = [];
        var {  
            _id,
            createdAt,
            totalNew,
            totalPartial,
            totalUpdated,
            totalFailedExtracted,
            totalFailedCreated,
            totalFailedRequest,
        } = item;
        const date = new Date(createdAt).toLocaleString();
        totalNew > 0 && details.push(totalNew + ' new data added');
        totalPartial > 0 && details.push(totalPartial + '(broken/partial) new data added');
        totalUpdated > 0 && details.push(totalUpdated + ' data updated');
        totalFailedExtracted > 0 && details.push(totalFailedExtracted + ' data failed to be extracted');
        totalFailedCreated > 0 && details.push(totalFailedCreated + ' data failed to be created');
        totalFailedRequest > 0 && details.push(totalFailedRequest + ' request failed due to network error');        
        return (
            <MDBListGroupItem key={_id} className="position-relative">                        
                <MDBRow className="pe-3 condensed">
                    <MDBCol sm="4" className="fw-bold text-truncate">{date}</MDBCol>
                    <MDBCol sm="8" className="ps-3 ps-sm-0 text-truncate">
                        {
                            details.length > 0 ? 
                            details.map(detail => <span>{ detail }<br/></span>) : 
                            <span>Everything is up to date</span>
                        }
                    </MDBCol>
                </MDBRow>                    
            </MDBListGroupItem>
        )
    }

    useEffect(() => {
        loadData();
        return () => {
            setError(true);
        }
    }, []);

    

    return (
        <div className="pt-3">            
            <Loading full loading={loading} error={error} reloadData={loadData}>
                
                <MDBListGroup>
                    <MDBListGroupItem>
                        Update history
                    </MDBListGroupItem>
                    {data.map((item, index) => (
                        <History item={item}/>
                    ))}
                </MDBListGroup>
            </Loading>
        </div>
    );
}
