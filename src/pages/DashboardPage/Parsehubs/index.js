// main
import React, { useState, useContext, useEffect } from "react";
import { AppContext } from "../../../App";

// components
import Loading from "../../../components/Loading";
import ParsehubForm from "./ParsehubForm";
import HistoryList from "./HistoryList";
import {
    MDBContainer,
    MDBListGroup,
    MDBListGroupItem,    
    MDBBadge,
    MDBBtn,
    MDBRow,
    MDBCol,
} from "mdb-react-ui-kit";

export default function ParsehubPage() {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState([]);
    const [ form, setForm ] = useState({ isEnabled: false, index: -1 });
    const { apiRequest, setToast } = useContext(AppContext);

    function loadData() {
        setLoading(true);
        setError(false);
        setData([]);
        apiRequest({
            method: "get",
            endpoint: "/parsehubs",
            setData,
            setError,
            setLoading,
        });
    }

    function updateFromAPI() {
        setLoading(true);
        apiRequest(
            {
                method: "get",
                endpoint: "/parsehubs/lastRun",
            },
            (err, response) => {
                if (err) {
                    setError(true);
                } else {                    
                    const {
                        totalNew,
                        totalUpdated,
                        totalPartial,
                        totalFailedExtracted,
                        totalFailedCreated,
                        totalFailedRequest                        
                    } = response.data;
                    var message = [];
                    totalNew > 0 && message.push(totalNew + ' new data added');
                    totalPartial > 0 && message.push(totalPartial + '(broken/partial) new data added');
                    totalUpdated > 0 && message.push(totalUpdated + ' data updated');
                    totalFailedExtracted > 0 && message.push(totalFailedExtracted + ' data failed to be extracted');
                    totalFailedCreated > 0 && message.push(totalFailedCreated + ' data failed to be created');
                    totalFailedRequest > 0 && message.push(totalFailedRequest + ' request failed due to network error');
                    setToast(message.join(', '));
                }
                setLoading(false);
            }
        );
    }

    function showForm(index) {        
        if (form.isEnabled) {
            setForm({ isEnabled: false, index });            
            return;
        }
        setForm({ isEnabled: true, index });
    }    

    const ParsehubList = ({data}) => (
        <MDBListGroup>
            <MDBListGroupItem>
                <MDBRow className="pe-3">
                    <MDBCol size="3">Name</MDBCol>
                    <MDBCol size="3">Project Token</MDBCol>
                    <MDBCol size="3">API Key</MDBCol>
                    <MDBCol size="3">Purpose</MDBCol>
                </MDBRow>
            </MDBListGroupItem>
            {data.map((item, index) => (
                <MDBListGroupItem key={item._id} className="position-relative">
                    <MDBBtn
                        className="text-danger fw-bold p-3 m-0 w-100 d-flex justify-content-end d-inline-block shadow-0 position-absolute top-0 end-0 bottom-0 start-0"
                        color="transparent"
                        onClick={() => showForm(index)}
                        style={{ zIndex: "3" }}
                    >
                        <i className="fa fa-pen"></i><span className="ms-2 d-none d-sm-block">Edit</span>
                    </MDBBtn>
                    <MDBRow className="pe-3 condensed">
                        <MDBCol size="3" className="fw-bold text-truncate">{item.name}</MDBCol>
                        <MDBCol size="3" className="text-truncate">{item.projectToken}</MDBCol>
                        <MDBCol size="3" className="text-truncate">{item.apiKey}</MDBCol>
                        <MDBCol size="3">{item.purpose}</MDBCol>
                    </MDBRow>                    
                </MDBListGroupItem>
            ))}
        </MDBListGroup>
    )

    useEffect(() => {
        loadData();
        return () => {
            setError(true);
        }
    }, []);

    return (
        <Loading full loading={loading} error={error} reloadData={loadData}>
            <MDBContainer
                fluid
                className="py-2 py-lg-4 px-3 px-lg-4"
                style={{ maxWidth: "840px" }}
            >
                { 
                    form.isEnabled && 
                    <ParsehubForm 
                        show={form.isEnabled}
                        data={data[form.index]}
                        reloadData={loadData}
                        toggleShow={ () => showForm(form.index) }
                    />
                }
                <div className="d-flex align-items-center mx-1 mb-4">
                    <small className="m-0">
                        Database will be automatically synced to Parsehub API every 3 days at 00:00 (UTC+7)
                    </small>
                    <MDBBtn
                        size="sm"
                        color="dark"
                        className="ms-auto"
                        rounded
                        onClick={updateFromAPI}
                    >
                        <i className="fa fa-sync-alt"></i>
                        <span className="ms-2 d-none d-sm-inline-block">Sync Now</span>
                    </MDBBtn>
                </div>
                <ParsehubList data={data}/>  
                <HistoryList />
                <div className="d-flex mt-4 justify-content-end">
                    <MDBBtn rounded className="position-fixed bottom-0 me-2 mb-3" 
                        style={{ zIndex: 5, boxShadow: '0px 2px 8px rgba(0,0,0,0.85)' }}
                        onClick={ () => showForm(-1) }
                        >
                        <i className="fa fa-plus me-2"></i> Add Data
                    </MDBBtn>
                </div>      
            </MDBContainer>
        </Loading>
    );
}
