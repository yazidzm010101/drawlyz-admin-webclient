// main
import React, {  useState, useEffect, useContext} from "react";
import { AppContext } from '../../../App';

// components
import ConfirmationForm from "../../../components/ConfirmationForm";
import {    
    MDBInput,    
    MDBBtn,
    MDBModal,
    MDBModalDialog,
    MDBModalContent,
    MDBModalHeader,
    MDBModalTitle,
    MDBModalBody,
    MDBModalFooter,
} from "mdb-react-ui-kit";

export default function ParsehubForm({ data, reloadData, toggleShow }) {    
    
    const { apiRequest, setToast } = useContext(AppContext);
    const [form, setForm] = useState(data);
    const [show, setShow] = useState(false);
    const [confirm, setConfirm] = useState(false);
    const title = data ? data.name : 'New Parsehub API';
    const method = data ? 'PUT' : 'POST';
    

    function updateForm(e) {        
        setForm({ ...form, [e.target.name]: e.target.value });
    }

    function submitForm(e) {
        e.preventDefault();        
        apiRequest({ method, endpoint:'/parsehubs', data: form }, (err, response) => {
            if(err) {
                setToast('Failed to save changes');
                return;
            }
            if(response.status === 200) {
                setToast('Changes saved successfully');
                hideForm();
                setTimeout(() => {                    
                    reloadData();
                }, 200);
            }
        });
    }    

    function hideForm() {
        setShow(false);
        setTimeout(() => {
            toggleShow();
        }, 200);
    }

    useEffect(() => {    
        setTimeout(() => {
            setShow(true);
        }, 5);        
    }, []);

    const submitPromise = () => new Promise((resolve, reject) => {
        apiRequest({method: 'DELETE', endpoint:'/parsehubs', data: { _id: form._id } }, (err, response) => {
            if(err) {
                setToast('Failed to delete data');
                return;
            }
            if(response.status === 200) {
                setToast('Data deleted successfully');
                hideForm();
                setTimeout(() => {                    
                    reloadData();
                }, 200);
            }
        });
    });

    const {
        name,
        projectToken,
        apiKey,
        purpose      
    } = form || {};

    return (
        <>
            {
                confirm && 
                <ConfirmationForm 
                    show={confirm} 
                    toggleShow={() => setConfirm(!confirm)}
                    submitPromise={submitPromise}
                    title="Confirmation"
                    body={ `Deleted data can't be restored. To confirm please enter "${name}" to confirm data deletion.` }
                    label="Name"
                    confirmation={name}
                    />
            }
            <MDBModal show={show} onClick={hideForm}>
                <form onSubmit={submitForm}>
                    <MDBModalDialog onClick={(e) => e.stopPropagation()}>
                        <MDBModalContent>
                            <MDBModalHeader>
                                <MDBModalTitle>{title}</MDBModalTitle>
                                {
                                    data && 
                                    <MDBBtn color="transparent" className="text-danger shadow-0" onClick={() => setConfirm(true)}>
                                    Delete<i className="fa fa-trash ms-2"></i>
                                    </MDBBtn>
                                }
                            </MDBModalHeader>
                            <MDBModalBody>                            
                                <MDBInput
                                    label="Name"
                                    className="active mt-4"
                                    labelClass="required"
                                    name="name"
                                    type="text"
                                    value={name || ""}
                                    onChange={updateForm}
                                    required
                                />
                                <MDBInput
                                    label="Project Token"
                                    className="active mt-4"                                    
                                    name="projectToken"
                                    textarea
                                    value={projectToken || ""}
                                    onChange={updateForm}                                 
                                />
                                <MDBInput
                                    label="API Key"
                                    className="active mt-4"                                    
                                    name="apiKey"
                                    textarea
                                    value={apiKey || ""}
                                    onChange={updateForm}                                 
                                />                                
                            </MDBModalBody>

                            <MDBModalFooter>
                                <MDBBtn color="dark" onClick={hideForm}>
                                    Close
                                </MDBBtn>
                                <MDBBtn type="submit">Save Changes</MDBBtn>
                            </MDBModalFooter>
                        </MDBModalContent>
                    </MDBModalDialog>
                </form>
            </MDBModal>
        </>
    );    
}