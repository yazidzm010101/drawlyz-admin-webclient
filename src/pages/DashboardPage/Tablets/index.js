// main
import React, { useState, useContext, useEffect } from "react";
import { AppContext } from "../../../App";
import { ArrayState } from "../../../utils";

// components
import Loading from "../../../components/Loading";
import TabletForm from "./TabletForm";
import TabletList from "./TabletList";
import {
    MDBContainer,
    MDBTabs,
    MDBTabsItem,
    MDBTabsLink,
    MDBTabsContent,
    MDBTabsPane,
    MDBBadge,    
    MDBBtn,    
} from "mdb-react-ui-kit";

// stylesheet
import "../../../stylesheets/pentablet.css";

export default function TabletPage() {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState([]);
    const [keyword, setKeyword] = useState("");
    const [form, setForm] = useState({ isEnabled: false, index: -1 });
    const [activeTab, setActiveTab] = useState(0);
    const { apiRequest } = useContext(AppContext);

    function loadData() {
        setLoading(true);
        setError(false);
        setData([]);
        apiRequest({
            method: "get",
            endpoint: "/brands",
            setData: (newData) =>
                setData([
                    { title: "All", queryString: "", length: 0 },
                    ...newData.map((title) => {
                        return { title, queryString: "?brand=" + title , length: 0};
                    }),
                    { title: "Incomplete", queryString: "?incomplete=true", length: 0 },
                ]),
            setError,
            setLoading,
        });
    }

    async function setLength(index, length) {                
        setData((prevState) => ArrayState.update(prevState, index, {
            ...data[index],
            length
        }));    
    }

    function showForm(index) {
        if (form.isEnabled) {
            setForm({ isEnabled: false, index });
            return;
        }
        setForm({ isEnabled: true, index });
    }

    useEffect(loadData, []);

    return (
        <Loading full loading={loading} error={error} reloadData={loadData}>
            <MDBContainer
                fluid
                className="px-0 pt-2 pt-lg-4 d-flex flex-column h-100 w-100"
                style={{ maxHeight: "100%", overflow: "hidden" }}
            >
                {form.isEnabled && (
                    <TabletForm
                        show={form.isEnabled}
                        reloadData={loadData}
                        toggleShow={() => showForm(form.index)}
                    />
                )}
                <div className="tabbable w-100 px-3 px-lg-4">
                    <MDBTabs className="d-flex">
                        {data.map(({ title, length }, index) => (
                            <MDBTabsItem key={title + "_" + index}>
                                <MDBTabsLink
                                    active={index === activeTab}
                                    onClick={() => setActiveTab(index)}
                                >
                                    {
                                        length > 0 && 
                                        <MDBBadge className="me-2 d-inline-block align-middle">{ length }</MDBBadge>
                                    }
                                    <span className="d-inline-block align-middle">
                                        {title}
                                    </span>
                                </MDBTabsLink>
                            </MDBTabsItem>
                        ))}
                    </MDBTabs>
                </div>
                <MDBTabsContent
                    style={{ flex: "1 1 auto", overflow: "hidden" }}
                >
                    {data.map(({ title, queryString }, index) => (
                        <MDBTabsPane
                            className="h-100"
                            key={"pane" + "_" + title}
                            show={index == activeTab}
                        >
                            <TabletList
                                keywordState={{ keyword, setKeyword }}
                                queryString={queryString}
                                setLength={(length) => setLength(index, length)}
                                reloadData={loadData}
                            />
                        </MDBTabsPane>
                    ))}
                </MDBTabsContent>
                <div className="d-flex px-3 px-lg-4 justify-content-end">
                    <MDBBtn
                        rounded
                        className="position-fixed bottom-0 me-4 mb-3"
                        style={{
                            zIndex: 5,
                            boxShadow: "0px 2px 8px rgba(0,0,0,0.85)",
                        }}
                        onClick={() => showForm(-1)}
                    >
                        <i className="fa fa-plus me-2"></i> Add Data
                    </MDBBtn>
                </div>
            </MDBContainer>
        </Loading>
    );
}
