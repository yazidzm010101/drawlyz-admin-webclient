import React, { useState, useContext, useEffect } from "react";
import { AppContext } from "../../../App";

import Loading from "../../../components/Loading";
import TabletCard from "./TabletCard";
import TabletForm from "./TabletForm";
import {
    MDBContainer,
    MDBRow,    
    MDBCol,
    MDBBadge,
    MDBInput,
} from "mdb-react-ui-kit";

import "../../../stylesheets/pentablet.css";

export default function TabletList({
    queryString,
    reloadData,
    setLength,
    keywordState: { keyword, setKeyword },
}) {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState([]);
    const [form, setForm] = useState({ isEnabled: false, index: -1 });
    const [useHeader, setUseHeader] = useState(true);
    const [y, setY] = useState(0);
    const { apiRequest } = useContext(AppContext);

    function loadData() {
        setLoading(true);
        setError(false);
        setData([]);
        apiRequest({
            method: "get",
            endpoint: "/tablets" + queryString,
            setData: (data) => {
                setData(data);
                setLength(data.length);
            },
            setError,
            setLoading,
        });
    }

    function handleNavigation(e) {
        const window = e.currentTarget;
        if((window.scrollTop - y) >= 128) {
            setUseHeader(false);
            setY(window.scrollTop);
            return;
        }
        if((window.scrollTop - y) <= -128) {
            setUseHeader(true);
            setY(window.scrollTop);
            return;
        }
      };

    function showForm(index) {
        if (form.isEnabled) {
            setForm({ isEnabled: false, index });
            return;
        }
        setForm({ isEnabled: true, index });
    }

    function showHint() {
        return (
            <MDBContainer
                className="mt-4 mb-5"
                fluid
                style={{ maxWidth: "480px" }}
            >
                <p className="text-center">
                    To add data click{" "}
                    <MDBBadge pill color="dark">
                        <i className="fa fa-plus me-2"></i>Add Data
                    </MDBBadge>{" "}
                    button
                </p>
            </MDBContainer>
        );
    }

    function showHeader() {
        return (
            <div className={'d-flex flex-wrap px-3 px-lg-4 py-2 overhide' + (useHeader ? '' : ' hidden')}
                style={{ flex: "0 0 auto" }}>
                <div className="mx-1 mb-3 me-auto">
                    Displaying Total {data.length} data.
                    {queryString.match(/incomplete/i) &&
                        "Incomplete data will not visible to public"}
                </div>
                <div
                    className="me-3 w-100"
                    style={{  maxWidth: "576px" }}
                >
                    <MDBInput
                        className="w-100"
                        name="keyword"
                        label="Search Tablet"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                    />
                </div>
            </div>
        );
    }

    function showList() {
        const dataFilter = keyword
            ? data.filter((item) => {
                  const name = (item.brand + item.model)
                      .replace(/ /g, "")
                      .toLowerCase();
                  const nameMatch = keyword.replace(/ /g, "").toLowerCase();
                  return name.includes(nameMatch);
              })
            : data;
        return (
            <>
                <MDBRow className="mx-0 px-2 px-lg-3">
                    {dataFilter.map((item, index) => (
                        <MDBCol key={item._id} xl="6" className="my-2">
                            <TabletCard
                                data={item}
                                showForm={() => showForm(index)}
                            />
                        </MDBCol>
                    ))}
                </MDBRow>
                {!queryString.match(/incomplete/) && showHint()}
                {form.isEnabled && (
                    <TabletForm
                        show={form.isEnabled}
                        data={dataFilter[form.index]}
                        reloadData={reloadData}
                        toggleShow={() => showForm(form.index)}
                    />
                )}
            </>
        );
    }

    function showNoData() {
        return (
            <div
                className={
                    "h-100 w-100 d-flex flex-column justify-content-center align-items-center"
                }
            >
                <i className="fas fa-tablet fa-5x text-danger"></i>
                <p className="my-2">There is no data yet.</p>
                {!queryString.match(/incomplete/) && showHint()}
            </div>
        );
    }

    useEffect(() => {
        loadData();
        return () => {
            setError(true);
        }
    }, []);

    return (
        <Loading full loading={loading} error={error} reloadData={reloadData}>
            <div className="h-100 d-flex flex-column">
                {data.length > 0 && showHeader()}
                <div
                    onScroll={handleNavigation}
                    className="w-100"
                    style={{ overflowY: "auto", flex: "1 1 auto" }}
                >
                    {data.length > 0 ? showList() : showNoData()}
                </div>
            </div>
        </Loading>
    );
}
