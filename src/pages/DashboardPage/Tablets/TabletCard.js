import {
    MDBCard,
    MDBCardTitle,
    MDBCardText,
    MDBCardBody,
    MDBRow,
    MDBCol,
    MDBBtn,
} from "mdb-react-ui-kit";

export default function TabletCard({ data, showForm }) {
    function specification(name, value) {
        return (
            <MDBCardText className="my-1">
                <span className="text-grey fw-bold text-muted">
                    {name + ": "}
                </span>
                <span className="fw-bold">{value}</span>
            </MDBCardText>
        );
    }

    const image = (
        <div
            className="rounded w-100"
            style={{ display: "inline-block", maxWidth: "128px" }}
        >
            {data.image ? (
                <div
                    className="rounded w-100"
                    style={{
                        paddingTop: "100%",
                        background: "white",
                        backgroundSize: "contain",
                        backgroundPosition: 'center',
                        backgroundRepeat: 'no-repeat',
                        backgroundImage: `url("${data.image}")`,
                    }}
                ></div>
            ) : (
                <div
                    className="rounded w-100 position-relative"
                    style={{
                        paddingTop: "75%",
                        background: "white",
                    }}
                >
                    <i className="fa fa-tablet fa-3x position-absolute top-50 start-50"
                    style={{
                        transform: 'translate(-50%, -50%) rotate(90deg)'
                    }}
                    ></i>
                </div>
            )}
        </div>
    );

    const price = 
        <MDBCardText>
            <span className="me-2 fw-bold">
                USD{" "}
                <span>
                    {data.price ? 
                        data.price.toString()
                        .replace(/([0-9](?=(?:[0-9]{3})+(?![0-9])))/g, "$1,") : '-'}
                </span>
            </span>            
            <small className="me-2 fw-bold">
                IDR{" "}
                <span>
                    {data.priceIDR ?
                        data.priceIDR.toString()
                        .replace(/([0-9](?=(?:[0-9]{3})+(?![0-9])))/g, "$1,") : '-'}
                </span>
            </small>
        </MDBCardText>
    ;

    const title = 
        <MDBCardTitle className={`fw-bold text-truncate`}>
            {data.brand} {data.model}{" "}
            <small>
                <i>({data.type})</i>
            </small>
        </MDBCardTitle>
    ;

    return (
        <MDBCard className="pentablet h-100 overflow-hidden my-2 condensed position-relative">
            <MDBRow className="g-0 h-100">
                <MDBCol size="12" sm="3" className="text-center p-2">
                    {image}                    
                    <MDBBtn
                        className="text-light fw-bold p-3 m-0 w-100 d-flex justify-content-end d-inline-block shadow-0 position-absolute top-0 end-0 bottom-0 start-0"
                        color="transparent"
                        onClick={showForm}
                        style={{ zIndex: "3" }}
                    >
                        <h6 className="fw-bold">
                            <i className="fa fa-pen me-1" /> Edit
                        </h6>
                    </MDBBtn>
                </MDBCol>                
                <MDBCol sm="9">
                    <MDBCardBody>
                        { title }
                        <small>
                            {data.type === "Pen Display"
                                ? specification(
                                      "Resolution",
                                      data.resolutionX +
                                          " x " +
                                          data.resolutionY
                                  )
                                : null}
                            {specification(
                                "Dimension",
                                data.dimensionX +
                                    " x " +
                                    data.dimensionY +
                                    " x " +
                                    data.dimensionZ +
                                    "mm"
                            )}
                            {specification(
                                "Working Area",
                                data.workingAreaX +
                                    " x " +
                                    data.workingAreaY +
                                    "mm (" +
                                    data.workingAreaDiagonalInch +
                                    '")'
                            )}
                            {specification(
                                "Pressure Levels",
                                data.pressureLevels
                            )}
                            {specification(
                                "Featured Controls",
                                data.featuredControls.replace('+', ' + ') || '-'
                            )}
                        </small>
                    </MDBCardBody>
                </MDBCol>
                <MDBCol className="text-end pe-2">
                    { price }
                </MDBCol>
            </MDBRow>
        </MDBCard>
    );
}
