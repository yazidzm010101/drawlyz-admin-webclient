// main
import React, { useState, useEffect, useContext } from "react";
import { AppContext } from "../../../App";

// components
import ConfirmationForm from "../../../components/ConfirmationForm";
import {
    MDBRow,
    MDBCol,
    MDBInput,
    MDBFile,
    MDBBtn,
    MDBModal,
    MDBModalDialog,
    MDBModalContent,
    MDBModalHeader,
    MDBModalTitle,
    MDBModalBody,
    MDBModalFooter,
} from "mdb-react-ui-kit";

export default function TabletForm({ data, reloadData, toggleShow }) {
    const { apiRequest, setToast } = useContext(AppContext);
    const [form, setForm] = useState(data);
    const [show, setShow] = useState(false);
    const [confirm, setConfirm] = useState(false);
    const title = data ? data.brand + " " + data.model : "New Graphic Tablet";
    const method = data ? "PUT" : "POST";

    function updateForm(e) {
        setForm({ ...form, [e.target.name]: e.target.value });
    }

    function updateFile(e) {
        setForm({ ...form, [e.target.name]: e.target.files[0] });
    }

    function submitForm(e) {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(form).forEach((item) => {
            formData.append(item, form[item]);
        });
        apiRequest(
            {
                method,
                endpoint: "/tablets",
                data: formData,
                headers: { "Content-Type": "multipart/form-data" },
            },
            (err, response) => {
                if (err) {
                    setToast("Failed to save changes");
                    return;
                }
                if (response.status === 200) {
                    setToast("Changes saved successfully");
                    hideForm();
                    setTimeout(() => {
                        reloadData();
                    }, 200);
                }
            }
        );
    }

    function hideForm() {
        setShow(false);
        setTimeout(() => {
            toggleShow();
        }, 200);
    }

    useEffect(() => {
        !data &&
            setForm({
                type: "Pen Tablet",
            });
        setTimeout(() => {
            setShow(true);
        }, 5);
    }, []);

    const submitPromise = () =>
        new Promise((resolve, reject) => {
            apiRequest(
                {
                    method: "DELETE",
                    endpoint: "/tablets",
                    data: { _id: form._id },
                },
                (err, response) => {
                    if (err) {
                        setToast("Failed to delete data");
                        return;
                    }
                    if (response.status === 200) {
                        setToast("Data deleted successfully");
                        hideForm();
                        setTimeout(() => {
                            reloadData();
                        }, 200);
                    }
                }
            );
        });

    const {
        url,
        brand,
        model,
        type,
        resolutionX,
        resolutionY,
        dimensionX,
        dimensionY,
        dimensionZ,
        workingAreaX,
        workingAreaY,
        pressureLevels,
        featuredControls,
        price,
    } = form || {};

    return (
        <>
            {confirm && (
                <ConfirmationForm
                    show={confirm}
                    toggleShow={() => setConfirm(!confirm)}
                    submitPromise={submitPromise}
                    title="Confirmation"
                    body={`Deleted data can't be restored. To confirm please enter "${brand} ${model}" to confirm data deletion.`}
                    label="Brand & Model"
                    confirmation={brand + " " + model}
                />
            )}
            <MDBModal show={show} onClick={hideForm}>
                <form onSubmit={submitForm}>
                    <MDBModalDialog onClick={(e) => e.stopPropagation()}>
                        <MDBModalContent>
                            <MDBModalHeader>
                                <MDBModalTitle>{title}</MDBModalTitle>
                                {data && (
                                    <MDBBtn
                                        color="transparent"
                                        className="text-danger shadow-0"
                                        onClick={() => setConfirm(true)}
                                    >
                                        Delete
                                        <i className="fa fa-trash ms-2"></i>
                                    </MDBBtn>
                                )}
                            </MDBModalHeader>
                            <MDBModalBody>
                                <MDBFile
                                    className="mb-2"
                                    name="image"
                                    label="Update image"
                                    onChange={updateFile}
                                />
                                <MDBInput
                                    label="URL"
                                    className="active mt-4 mb-2"
                                    labelClass="required"
                                    name="url"
                                    type="text"
                                    required
                                    value={url || ""}
                                    onChange={updateForm}
                                />
                                <MDBRow className="g-3 my-2">
                                    <MDBCol size="5">
                                        <MDBInput
                                            label="Brand"
                                            className="active"
                                            labelClass="required"
                                            name="brand"
                                            type="text"
                                            required
                                            value={brand || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                    <MDBCol size="7">
                                        <MDBInput
                                            label="Model"
                                            className="active"
                                            labelClass="required"
                                            name="model"
                                            type="text"
                                            required
                                            value={model || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                </MDBRow>
                                <MDBRow className="g-3 my-2">
                                    <MDBCol size="7">
                                        <div className="position-relative">
                                            <select
                                                defaultValue={
                                                    type || "Pen Tablet"
                                                }
                                                className="form-select"
                                                name="type"
                                                required
                                                onChange={updateForm}
                                                style={{
                                                    paddingTop: "0.26em",
                                                    paddingBottom: "0.26em",
                                                }}
                                            >
                                                <option value="Pen Tablet">
                                                    Pen Tablet
                                                </option>
                                                <option value="Pen Display">
                                                    Pen Display
                                                </option>
                                            </select>
                                            <label
                                                className="position-absolute required px-1 bg-white left-0 bottom-100"
                                                style={{
                                                    transform:
                                                        "translate(8px, 50%)",
                                                    fontSize: "0.8em",
                                                }}
                                            >
                                                Type
                                            </label>
                                        </div>
                                    </MDBCol>
                                    <MDBCol size="5">
                                        <MDBInput
                                            label="Price (USD)"
                                            className="active"
                                            labelClass="required"
                                            name="price"
                                            type="number"
                                            required
                                            value={price || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                </MDBRow>
                                {type === "Pen Display" ? (
                                    <>
                                        <MDBRow className="g-3 mt-2">
                                            <div className="text-grey">
                                                Display Resolution
                                            </div>
                                            <MDBCol size="6">
                                                <MDBInput
                                                    label="Resolution X"
                                                    className="active"
                                                    labelClass="required"
                                                    name="resolutionX"
                                                    type="number"
                                                    required
                                                    value={resolutionX || ""}
                                                    onChange={updateForm}
                                                />
                                            </MDBCol>
                                            <MDBCol size="6">
                                                <MDBInput
                                                    label="Resolution Y"
                                                    className="active"
                                                    labelClass="required"
                                                    name="resolutionY"
                                                    type="number"
                                                    required
                                                    value={resolutionY || ""}
                                                    onChange={updateForm}
                                                />
                                            </MDBCol>
                                        </MDBRow>
                                    </>
                                ) : null}
                                <MDBRow className="g-3 mt-2">
                                    <div className="text-grey">
                                        Dimension (mm)
                                    </div>
                                    <MDBCol size="4">
                                        <MDBInput
                                            label="Width"
                                            className="active"
                                            labelClass="required"
                                            name="dimensionX"
                                            type="number"
                                            required
                                            value={dimensionX || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                    <MDBCol size="4">
                                        <MDBInput
                                            label="Height"
                                            className="active"
                                            labelClass="required"
                                            name="dimensionY"
                                            type="number"
                                            required
                                            value={dimensionY || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                    <MDBCol size="4">
                                        <MDBInput
                                            label="Thickness"
                                            className="active"
                                            labelClass="required"
                                            name="dimensionZ"
                                            type="number"
                                            required
                                            value={dimensionZ || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                </MDBRow>
                                <MDBRow className="g-3 mt-2">
                                    <div className="text-grey">
                                        Working Area (mm)
                                    </div>
                                    <MDBCol size="4">
                                        <MDBInput
                                            label="Width"
                                            className="active"
                                            labelClass="required"
                                            name="workingAreaX"
                                            type="number"
                                            required
                                            value={workingAreaX || ""}
                                            onChange={updateForm}
                                        />
                                    </MDBCol>
                                    <MDBCol size="4">
                                        <MDBInput
                                            label="Height"
                                            className="active"
                                            labelClass="required"
                                            name="workingAreaY"
                                            type="number"
                                            value={workingAreaY || ""}
                                            onChange={updateForm}
                                            required
                                        />
                                    </MDBCol>
                                    <MDBCol size="4">
                                        <MDBInput
                                            label="Diagonal (inch)"
                                            className="active"
                                            name="workingAreaDiagonalInch"
                                            type="number"
                                            onChange={updateForm}
                                            disabled
                                            value={
                                                Math.round(
                                                    (Math.sqrt(
                                                        Math.pow(
                                                            workingAreaX,
                                                            2
                                                        ) +
                                                            Math.pow(
                                                                workingAreaY,
                                                                2
                                                            )
                                                    ) /
                                                        25.4) *
                                                        10
                                                ) / 10
                                            }
                                            required
                                        />
                                    </MDBCol>
                                </MDBRow>
                                <MDBInput
                                    label="Pressure Levels"
                                    className="active mt-4"
                                    labelClass="required"
                                    name="pressureLevels"
                                    type="number"
                                    step="1024"
                                    value={pressureLevels || ""}
                                    onChange={updateForm}
                                    required
                                />
                                <MDBInput
                                    label="Featured Controls"
                                    className="active mt-4"
                                    name="featuredControls"
                                    textarea
                                    value={featuredControls || ""}
                                    onChange={updateForm}
                                />
                            </MDBModalBody>

                            <MDBModalFooter>
                                <MDBBtn color="dark" onClick={hideForm}>
                                    Cancel
                                </MDBBtn>
                                <MDBBtn type="submit">Save Changes</MDBBtn>
                            </MDBModalFooter>
                        </MDBModalContent>
                    </MDBModalDialog>
                </form>
            </MDBModal>
        </>
    );
}
