import React, { useState, useContext, useEffect } from "react";
import { AppContext } from "../../../App";
import {
    MDBContainer,
    MDBListGroup,
    MDBListGroupItem,
    MDBBadge,
    MDBBtn,
} from "mdb-react-ui-kit";
import Loading from "../../../components/Loading";
import AdministratorForm from "./AdministratorForm";

export default function AdminPage() {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState([]);
    const [form, setForm] = useState({ isEnabled: false, index: -1 });
    const { apiRequest, user } = useContext(AppContext);

    function loadData() {
        apiRequest({
            method: "get",
            endpoint: "/admins",
            setData,
            setError,
            setLoading,
        });
    }

    function showForm(index) {
        if (form.isEnabled) {
            setForm({ isEnabled: false, index });
            return;
        }
        setForm({ isEnabled: true, index });
    }

    const UserCircle = ({ name, className, style }) => {
        const hue = ((name.toUpperCase().charCodeAt(0) - 65) / 25) * 360;
        const background = `hsl(${hue}, 80%, 55%)`;
        return (
            <div className={"w-100 " + className} style={style}>
                <div
                    className="w-100 rounded-circle position-relative"
                    style={{ background, paddingTop: "100%" }}
                >
                    <h2 className="m-0 text-white position-absolute top-50 start-50 translate-middle">
                        {name[0].toUpperCase()}
                    </h2>
                </div>
            </div>
        );
    };

    const UserText = ({ user, className, style }) => (
        <span className={"d-flex flex-wrap " + className} style={style}>
            <h5
                className="text-truncate w-100 m-0 mb-1"
                style={{ minWidth: 0 }}
            >
                {user.name}{" "}
                <small className="fw-light text-muted">({user.username})</small>
            </h5>
            <span>
                <i className="fa fa-users-cog me-1"></i>
                <span className="text-muted">
                    {user.isMaster && " Master & "} Administrator
                </span>
            </span>
        </span>
    );

    const ButtonEditOverlay = ({ ...rest }) => (
        <MDBBtn
            className="fw-bold p-3 text-danger m-0 w-100 d-flex justify-content-end d-inline-block shadow-0 position-absolute top-0 end-0 bottom-0 start-0"
            color="transparent"
            style={{ zIndex: "3" }}
            {...rest}
        >
            <i
                className={`fa ${user.isMaster ? "fa-pen" : "fa-lock"} me-1`}
            ></i>
            <span className="ms-1 d-none d-sm-block">Edit</span>
        </MDBBtn>
    );

    const UserList = ({ data }) => {
        return (
            <MDBListGroup>
                {data.map((item, index) => (
                    <MDBListGroupItem
                        key={item._id}
                        className="position-relative condensed d-flex"
                    >
                        <ButtonEditOverlay
                            onClick={() => user.isMaster && showForm(index)}
                            disabled={!user.isMaster}
                        />
                        <UserCircle
                            name={item.name}
                            className="me-3 d-none d-sm-inline-block"
                            style={{ maxWidth: "56px", minWidth: "32px" }}
                        />
                        <UserText
                            user={item}
                            className="text-truncate pe-3 flex-grow-1"
                            style={{ minWidth: "75%" }}
                        />
                    </MDBListGroupItem>
                ))}
            </MDBListGroup>
        );
    };

    useEffect(() => {
        loadData();
        return () => {
            setError(true);
        }
    }, []);

    return (
        <Loading full loading={loading} error={error}>
            <MDBContainer
                fluid
                className="py-2 py-lg-4 px-3 px-lg-4"
                style={{ maxWidth: "840px" }}
            >
                {form.isEnabled && user.isMaster && (
                    <AdministratorForm
                        data={data[form.index]}
                        toggleShow={() => showForm(form.index)}
                        reloadData={loadData}
                    />
                )}
                <p className="flex-grow-1 mt-3">
                    Listing {data.length} administrators
                    {!user.isMaster && (
                        <>
                            <br />
                            <small className="text-muted">
                                <i className="fa fa-lock me-1"></i>You don't
                                have permission to edit this page
                            </small>
                        </>
                    )}
                </p>
                <UserList data={data} />
                <div className="d-flex mt-4 justify-content-end">
                    <MDBBtn
                        rounded
                        className="position-fixed bottom-0 me-2 mb-3"
                        style={{
                            zIndex: 5,
                            boxShadow: "0px 2px 8px rgba(0,0,0,0.85)",
                        }}
                        disabled={!user.isMaster}
                        onClick={() => user.isMaster && showForm(-1)}
                    >
                        <i
                            className={`fa ${
                                user.isMaster ? "fa-plus" : "fa-lock"
                            } me-2`}
                        ></i>{" "}
                        Add new administrator
                    </MDBBtn>
                </div>
            </MDBContainer>
        </Loading>
    );
}
