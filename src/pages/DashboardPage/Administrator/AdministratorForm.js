// main
import React, {  useState, useEffect, useContext} from "react";
import { AppContext } from '../../../App';

// components
import ConfirmationForm from "../../../components/ConfirmationForm";
import {    
    MDBInput,    
    MDBBtn,
    MDBModal,
    MDBModalDialog,
    MDBModalContent,
    MDBModalHeader,
    MDBModalTitle,
    MDBModalBody,
    MDBModalFooter,
    MDBRow,
    MDBCol,
    MDBCheckbox,
} from "mdb-react-ui-kit";

export default function AdministratorForm({ data, reloadData, toggleShow }) {    
    const { apiRequest, setToast, user, setUser } = useContext(AppContext);
    const [form, setForm] = useState(data);
    const [show, setShow] = useState(false);
    const [remove, setRemove] = useState(false);
    const [resetPassword, setResetPassword] = useState(false);
    const [confirm, setConfirm] = useState(false);
    const title = data ? data.name : 'New Administrator';
    const method = data ? 'PUT' : 'POST';
    

    function updateForm(e) {        
        setForm({ ...form, [e.target.name]: e.target.value });
    }

    function submitForm(e) {
        e.preventDefault();
        setConfirm(true);      
    }        

    function hideForm() {
        setShow(false);
        setTimeout(() => {
            toggleShow();
        }, 200);
    }

    useEffect(() => {    
        setTimeout(() => {
            setShow(true);
        }, 5);        
    }, []);

    const submitPromise= (confirmation) =>  new Promise((resolve, reject) => {
        var formData;
        if (remove)
            formData = { _id: data._id, confirmation, };
        else if (resetPassword || !data)
            formData = { ...form, confirmation, }
        else
            formData = { _id: data._id, name: form.name, username: form.username, confirmation, }
        const failedMessage = remove ? 'Failed to delete data' : 'Failed to save changes';
        const formMethod = remove ? 'DELETE' : method;
        const successMessage = remove ? 'Data deleted successfully': 'Changes saved successfully';
        apiRequest({ method: formMethod, endpoint:'/admins', data: formData }, (err, response) => {
            if(err && !err.response) {
                setToast(failedMessage);
                reject(err.response);
                return;
            }
            if(response.status === 200) {
                if (data) {
                    if (data.username == form.username && method == 'PUT') {
                        setUser({ ...user, name: form.name});
                    }
                }
                setToast(successMessage);
                setRemove(false);
                hideForm();
                setTimeout(() => {                    
                    reloadData();
                }, 200);
                resolve(response);
                return;
            }
            reject(err);
        });
    })     

    const {
        name,
        username,
        password,
    } = form || {};

    return (
        <>
            {
                confirm && 
                <ConfirmationForm 
                    show={confirm} 
                    toggleShow={() => setConfirm(!confirm)}
                    submitPromise={submitPromise}
                    title="Confirmation"
                    body={`You are about to ${remove ? 'delete' : 'changes'} restricted data, please enter your password to validate it's you`}
                    label="Password"
                    hideText/>
            }
            <MDBModal show={show} onClick={hideForm}>
                <form onSubmit={submitForm}>
                    <MDBModalDialog onClick={(e) => e.stopPropagation()}>
                        <MDBModalContent>
                            <MDBModalHeader>
                                <MDBModalTitle>{title}</MDBModalTitle>
                                { 
                                    data && (!data.isMaster && 
                                    <MDBBtn color="transparent" className="text-danger shadow-0" onClick={() => {setConfirm(true); setRemove(true)}}>
                                    Delete<i className="fa fa-trash ms-2"></i>
                                    </MDBBtn>)
                                }
                            </MDBModalHeader>
                            <MDBModalBody>                            
                                <MDBInput
                                    label="Name"
                                    className="active mt-4"
                                    labelClass="required"
                                    name="name"
                                    type="text"
                                    autoComplete="nope"
                                    value={name || ""}
                                    onChange={updateForm}
                                    required
                                />
                                <MDBInput
                                    label="Username"
                                    className="active mt-4"                                    
                                    name="username"
                                    type="text"
                                    autoComplete="nope"
                                    value={username || ""}
                                    onChange={updateForm}                                 
                                />
                                <MDBRow className="mt-4">
                                    <MDBCol size={ data ? 10 : 12 }>
                                        <MDBInput
                                            label={ data ? 'Reset password' : 'Password' }
                                            className="active"                                    
                                            name="password"
                                            type="password"
                                            autoComplete="new-password"
                                            value={password || ""}
                                            disabled={data && !resetPassword}
                                            onChange={updateForm}                                 
                                        />
                                    </MDBCol>
                                    {
                                        data && 
                                        <MDBCol size="2">
                                            <MDBCheckbox
                                                onChange={(e) => setResetPassword(e.target.checked)}
                                                autoComplete="nope"                                            
                                                />
                                        </MDBCol>
                                    }
                                    
                                </MDBRow>                                
                            </MDBModalBody>

                            <MDBModalFooter>
                                <MDBBtn color="dark" onClick={hideForm}>
                                    Close
                                </MDBBtn>
                                <MDBBtn type="submit">Save Changes</MDBBtn>
                            </MDBModalFooter>
                        </MDBModalContent>
                    </MDBModalDialog>
                </form>
            </MDBModal>
        </>
    );    
}