// main
import React, { useContext, useState } from "react";
import { AppContext } from '../../App';

// navigation and pages
import { Switch } from "react-router-dom";
import PrivateRoute from "../../routes/PrivateRoute";
import About from "./About";
import Administrator from "./Administrator";
import Criterias from "./Criterias";
import Parsehubs from "./Parsehubs";
import Tablets from "./Tablets";

// custom components
import Navbar from "../../components/Navbar";
import Sidebar from "../../components/Sidebar";
import Toast from "../../components/Toast";

export default function Dashboard() {
    const { user } = useContext(AppContext);
    const [ navToggled, toggleNav ] = useState(false);
    const navs = [
        {
            text: "Graphic Tablets",
            href: "/dashboard/tablets",
            className: "fa fa-tablet",
        },
        {
            text: "Criteria",
            href: "/dashboard/criterias",
            className: "fa fa-clipboard-list",
        },
        {
            text: "Parsehub API",
            href: "/dashboard/parsehub-apis",
            className: "fa fa-server",
        },
        {
            text: "Administrator",
            href: "/dashboard/admin-manager",
            className: "fa fa-user-cog",
        },
        {
            text: "About",
            href: "/dashboard/about",
            className: "fa fa-info-circle",
        },
    ];

    return (
        <div id="dashboard">
            <Navbar user={user} navToggled={navToggled} toggleNav={toggleNav} />
            <div className={"d-flex position-relative" + (navToggled && " toggled")} id="wrapper">
                <Sidebar navs={navs} />
                <div id="page-content-wrapper" style={{ background: "#212121" }}>
                    <div className="h-100 w-100 bg-white rounded-3" style={{ overflowY: "auto" }}>
                        <Toast context={AppContext} position="fixed" style={{ bottom: 0}}/>
                        <Switch>
                          <PrivateRoute path='/dashboard/tablets' component={Tablets} exact/>
                          <PrivateRoute path='/dashboard/criterias' component={Criterias} exact/>
                          <PrivateRoute path='/dashboard/admin-manager' component={Administrator} exact/>
                          <PrivateRoute path='/dashboard/parsehub-apis' component={Parsehubs} exact/>
                          <PrivateRoute path='/dashboard/about' component={About} exact/>
                        </Switch>
                    </div>
                </div>
            </div>
        </div>
    );
}
