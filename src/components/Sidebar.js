import React from "react";
import "../stylesheets/sidebar.css";
import { NavLink } from "react-router-dom";

export default function Sidebar({navs}) {
    return (
        <div className="bg-dark text-light" id="sidebar-wrapper">
            <div className="list-group list-group-flush">
                {navs.map((item, index) => (
                    <NavLink
                        to={item.href}
                        key={"nav-" + index}
                        activeClassName="active"
                        className="bg-dark text-light list-group-item list-group-item-action border-0 d-flex align-items-center"
                    >
                        <i
                            className={item.className + " pb-1 pe-2"}
                            style={{ width: "32px" }}
                        ></i>
                        {item.text}
                    </NavLink>
                ))}
            </div>
        </div>
    );
};