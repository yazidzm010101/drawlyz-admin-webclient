import { useState, useEffect, useContext, useRef } from "react";

export default function Toast({ context, position, style }) {
    const { toast, setToast } = useContext(context);
    const [message, setMessage] = useState(null);
    const [className, setClassName] = useState("");

    useEffect(() => {
        if (toast) {
            showToast();
            setTimeout(() => {
                hideToast();
            }, 4000);
        }        
    }, [toast]);

    function showToast() {
        setMessage(toast);
        setTimeout(() => {
            setClassName("show");
        }, 50);
    }

    function hideToast() {        
        setClassName("");
        setTimeout(() => {
            setMessage(null);            
        }, 200);
    }

    return (
        <div className="d-flex w-100 justify-content-end position-relative">
            <div
                className={`toast-container end-0 m-3 ${!toast && "d-none"}`}
                style={{ position, zIndex: 2000, maxWidth: "320px", ...style }}
            >
                <div
                    className={`${className} toast fade d-flex align-items-center bg-dark text-white border-0`}
                >
                    <div className="toast-body">
                        <span className="fw-bold">{message}</span>
                    </div>
                    <button
                        type="button"
                        className="btn-close btn-close-white ms-auto me-2"
                        onClick={hideToast}
                    />
                </div>
            </div>
        </div>
    );
}
