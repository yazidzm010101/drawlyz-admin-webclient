import { MDBBtn, MDBSpinner } from "mdb-react-ui-kit";

export default function Loading({loading, error, full, children, reloadData, size}) {    
    const className = full && 'h-100 w-100';

    if (loading) {
        return (
            <div className={ className + ' d-flex flex-column justify-content-center align-items-center'}>
                <MDBSpinner size={ size } color="danger" />                
                <p className="my-2">fetching data...</p>
            </div>                    
        )
    }

    if (!loading && error) {
        return (
            <div className={className + ' h-100 w-100 d-flex flex-column justify-content-center align-items-center'}>
                <i className="fas fa-frown fa-5x text-danger"></i>
                <p className="my-2">Ups, something went wrong, please reload the page</p>
                <MDBBtn color="dark" onClick={reloadData}>Try Again</MDBBtn>
            </div>
        )
    }

    return children;
}