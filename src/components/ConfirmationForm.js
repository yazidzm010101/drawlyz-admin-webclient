import React, { useState, useEffect, useContext } from "react";
import { AppContext } from "../App";
import {
    MDBInput,
    MDBBtn,
    MDBModal,
    MDBModalDialog,
    MDBModalContent,
    MDBModalHeader,
    MDBModalTitle,
    MDBModalBody,
    MDBModalFooter,
} from "mdb-react-ui-kit";

export default function ConfirmationForm({
    title,
    body,
    label,
    confirmation,
    hideText,
    toggleShow,
    submitPromise,
}) {
    const { setToast } = useContext(AppContext);
    const [input, setInput] = useState("");
    const [show, setShow] = useState(false);
    const [enabled, setEnabled] = useState(!confirmation);

    function updateForm(e) {
        setInput(e.target.value);
    }

    function hideForm() {
        setShow(false);
        setTimeout(() => {
            toggleShow();
        }, 200);
    }

    async function submitForm(e) {
        e.preventDefault();
        if (!enabled) {
            setToast("Validate your confirmation first!");
            return;
        }
        try {
            const hide = await submitPromise(input);
            if (hide) {
                hideForm();
            }
        } catch (err) {
            // do nothing
        }
    }

    useEffect(() => {
        setTimeout(() => {
            setShow(true);
        }, 5);
        return () => {
            setShow(false);
        };
    }, []);

    useEffect(() => {        
        confirmation && setEnabled(confirmation.replace(/\n/g, ' ') == input)
    }, [input]);

    return (
        <MDBModal
            show={show}
            onClick={hideForm}
            style={{
                zIndex: 3000,
                display: "block",
                background: "rgba(0, 0, 0, 0.5)",
            }}
        >
            <form onSubmit={submitForm} autoComplete="off">
                <MDBModalDialog
                    onClick={(e) => e.stopPropagation()}
                    style={{ maxWidth: "360px" }}
                >
                    <MDBModalContent>
                        <MDBModalHeader>
                            <MDBModalTitle>{title}</MDBModalTitle>
                            <MDBBtn
                                className="btn-close"
                                color="none"
                                onClick={hideForm}
                            ></MDBBtn>
                        </MDBModalHeader>
                        <MDBModalBody>
                            <p>{body}</p>
                            <MDBInput
                                label={label}
                                className="active mt-4"
                                required
                                name="confirmation"
                                type={hideText ? "password" : "text"}
                                value={input}
                                autoComplete={
                                    hideText ? "new-password" : "nope "
                                }
                                onChange={updateForm}
                            />
                        </MDBModalBody>

                        <MDBModalFooter>
                            <MDBBtn color="dark" onClick={hideForm}>
                                Close
                            </MDBBtn>
                            <MDBBtn type="submit" disabled={!enabled}>
                                Confirm
                            </MDBBtn>
                        </MDBModalFooter>
                    </MDBModalContent>
                </MDBModalDialog>
            </form>
        </MDBModal>
    );
}
