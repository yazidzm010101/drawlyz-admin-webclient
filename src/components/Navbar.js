// mediocre
import { useContext } from 'react';
import { AppContext } from '../App';
import cookie from "react-cookies";
// vendor's component
import {
    MDBDropdown,
    MDBDropdownMenu,
    MDBDropdownToggle,
    MDBDropdownItem,
    MDBDropdownLink,
    MDBBtn,
} from "mdb-react-ui-kit";

export default function Navbar({ user, navToggled, toggleNav }) {

    const { setUser, setToast } = useContext(AppContext);    

    function signOut() {
        cookie.remove("session", { path: "/" });
        setUser(null);
        setToast('Successfully signed out');
    }

    const NavToggler = () => (
        <MDBBtn
            id="menu-toggle"
            color="dark"
            className="shadow-0 px-2 py-1 me-2 mb-1"
            onClick={() => toggleNav(!navToggled)}
        >
            <h5 className="m-0">
                <i className="fa fa-bars"></i>
            </h5>
        </MDBBtn>
    );

    const AppName = () => (
        <a className="navbar-brand ms-0 me-auto">
            <span className="d-none fw-bold d-sm-inline-block">
                DRAWLYZ <sup className="text-primary-variant">admin</sup>
            </span>
        </a>
    );

    const UserInfo = () => (
        user && 
        <MDBDropdown group className="shadow-0">
            <MDBDropdownToggle color="dark">
                <i className="fa fa-user me-2"></i> {user.name}
            </MDBDropdownToggle>
            <MDBDropdownMenu>
                <MDBDropdownItem>
                    <MDBDropdownLink onClick={signOut} style={{ cursor: 'pointer' }}>
                        Sign out
                    </MDBDropdownLink>
                </MDBDropdownItem>
            </MDBDropdownMenu>
        </MDBDropdown>
    );

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <NavToggler/>
                <AppName/>
                <UserInfo/>
            </div>
        </nav>
    );
};