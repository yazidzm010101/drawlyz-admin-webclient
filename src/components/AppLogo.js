import logo from "../images/ic_launcher.png";

export default function AppName() {
    return (
        <div className="app-logo">
            <img
                className="img-fluid mx-3 my-1 w-100"
                src={logo}
                alt="logo"
                style={{ maxWidth: 96 + "px" }}
            />
            <h5 className="position-relative my-0 mx-auto">
                <strong>
                    DRAWLYZ
                    <sup className="ms-1 position-absolute right-0 mt-2 text-primary-variant">
                        admin
                    </sup>
                </strong>
            </h5>
        </div>
    );
}
