// mediocre
import React, { useState, useEffect } from "react";
import cookie from "react-cookies";
import axios from "axios";
// navigation and pages
import { BrowserRouter, Switch } from "react-router-dom";
import PublicRoute from "./routes/PublicRoute";
import PrivateRoute from "./routes/PrivateRoute";
import SigninPage from "./pages/SigninPage";
import DashboardPage from "./pages/DashboardPage";

export const AppContext = React.createContext({    
    baseUrl: null,
    user: null,        
    toast: null,    
    apiRequest: () => {},
    setUser: ()  => {},
    setToast: () => {},     
});

export const AppProvider = AppContext.Provider;
export const AppConsumer = AppContext.Consumer;

export default function App() {    
    const baseUrl = process.env.REACT_APP_MODE.toLowerCase() === "production" ? process.env.REACT_APP_API_URL : process.env.REACT_APP_API_DEVELOPMENT_URL;
    const [user, setUser] = useState(null);
    const [booting, setBooting] = useState(true);
    const [toast, setToast] = useState(null);    

    function apiRequest({ setLoading, setError, setData, endpoint, ...config }, callback) {
        typeof setLoading === 'function' && setLoading(true);        
        axios({            
            withCredentials: true,
            url: baseUrl + endpoint,
            ...config,
        }).then(response => {
            typeof setData === 'function' && setData(response.data);
            typeof setLoading === 'function' && setLoading(false);
            typeof callback === 'function' && callback(response.status !== 200, response);
        }).catch(err => {

            typeof setError === 'function' && setError(true);
            typeof setLoading === 'function' && setLoading(false);

            if (err.response) {
                if (err.response.status === 403) {
                    cookie.remove("session", { path: "/" });
                    setUser(null);                    
                    setToast('Your session has expired, please sign in again');                    
                }
                if (err.response.data) {
                    err.response.data.message && setToast(err.response.data.message);
                }
                typeof callback === 'function' && callback(err, err.response);
                return;
                
            }            
            typeof callback === 'function' && callback(err, null);
            console.log(err);
        })
    }    

    useEffect(() => {        
        axios
            .get(baseUrl + "/me", {
                withCredentials: true,
            })
            .then((response) => {
                if (response.status === 200) {                      
                    setUser(response.data.user);
                    setToast(response.data.message);
                    setBooting(false);
                }
            })
            .catch((err) => {                  
                setBooting(false);
            });            
    }, []);
    
        
    return (
        !booting && 
        <AppProvider value={{ baseUrl, user, toast, apiRequest, setUser, setToast }}>
            <BrowserRouter>
                <Switch>
                    <PublicRoute path='/' exact restricted={true} component={SigninPage}/>
                    <PublicRoute path='/sign-in' exact restricted={true} component={SigninPage}/>
                    <PrivateRoute path="/dashboard" component={DashboardPage}/>
                </Switch>
            </BrowserRouter>
        </AppProvider>
    )    
}