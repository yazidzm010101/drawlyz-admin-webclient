import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { AppContext } from '../App';

export default function PrivateRoute({ component: Component, ...rest }) {

    const { user } = useContext(AppContext);
    
    function render(props) {
        return user !== null ? (
            <Component user={user} {...props} />
        ) : (
            <Redirect to="/sign-in" />
        );
    }

    return (        
        <Route {...rest} render={render}/>
    );
};
