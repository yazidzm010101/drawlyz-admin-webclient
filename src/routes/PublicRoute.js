import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { AppContext } from '../App';

export default function PublicRoute ({ component: Component, restricted, ...rest }){

    const { user } = useContext(AppContext);

    function render(props) {
        return !(user !== null && restricted) ? (
            <Component user={user} {...props}/>
        ) : (
            <Redirect to="/dashboard/tablets"/>
        );
    }
    
    return (
        <Route {...rest} render={render}/>
    );
    
};